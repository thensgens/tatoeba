# Tatoeba

Tatoeba provides a go package for querying example sentences from the 'tatoeba project'. (see https://tatoeba.org/jpn/)
Additionally, there is also a protobuf file defining all necessary types and services (see https://developers.google.com/protocol-buffers).

Currently, the repository https://gitlab.com/thensgens/tatoeba-server implements a grpc server based on protobuf.