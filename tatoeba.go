package tatoeba

import (
	"encoding/csv"
	"errors"
	"github.com/jszwec/csvutil"
	"io"
	"log"
	"os"
	"strings"
)

// This simple implementation reads all data initially from a provided file (.tsv or .csv) and keeps
// all data in memory.
type InMem struct {
	data []Entry
}

type Entry struct {
	//TODO: Idx is only current temporary field name. Check Tatoeba documentation for actual purpose of this field.
	Idx      int
	Lang     string
	Sentence string
}

type SentenceReader interface {
	Get(search string) ([]string, error)
}

type SentenceWriter interface {
	Put(input string) error
	Delete(input string) error
	Update(input string) error
}

type SentenceReaderWriter interface {
	SentenceReader
	SentenceWriter
}

func NewInMem(fileName string) SentenceReaderWriter {
	file, err := os.Open(fileName)
	if err != nil {
		log.Fatal(err)
	}
	return &InMem{data: unmarshalCSV(file)}
}

func (tmem *InMem) Get(search string) ([]string, error) {
	var res []string
	for _, e := range tmem.data {
		if strings.Contains(e.Sentence, search) {
			res = append(res, e.Sentence)
		}
	}
	return res, nil
}

func (tmem *InMem) Put(input string) error {
	return errors.New("pointer receiver function not implemented yet")
}

func (tmem *InMem) Delete(input string) error {
	return errors.New("pointer receiver function not implemented yet")
}

func (tmem *InMem) Update(input string) error {
	return errors.New("pointer receiver function not implemented yet")
}

func unmarshalCSV(reader io.Reader) []Entry {
	r := csv.NewReader(reader)
	r.Comma = '\t'

	userHeader, err := csvutil.Header(Entry{}, "csv")
	if err != nil {
		log.Fatal(err)
	}

	// This custom header is needed because the input csv does not provide a header.
	dec, err := csvutil.NewDecoder(r, userHeader...)
	if err != nil {
		log.Fatal(err)
	}

	var entries []Entry
	for {
		var e Entry
		if err := dec.Decode(&e); err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}
		entries = append(entries, e)
	}

	return entries
}